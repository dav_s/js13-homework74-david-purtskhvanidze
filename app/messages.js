const express = require('express');
const db = require('../fileDB');

const router = express.Router();

router.get('/', (req, res) => {
    const messages = db.getItems();
    return res.send(messages);
});

router.post('/', (req, res) => {
    const message = {
        message: req.body.message
    }
    db.addItem(message);
    return res.send(message);
});

module.exports = router;
const fs = require('fs');
const path = './messages';

let data = [];

module.exports = {
    init() {
        try {
            fs.readdir(path, (err, files) => {
                files.forEach(file => {
                    const fileContents = fs.readFileSync(path + '/' + file);
                    data.push(JSON.parse(fileContents));
                });
            })
        } catch (e) {
            data = [];
        }
    },

    getItems() {
        return data.slice(-5);
    },

    addItem(item) {
        item.datetime = new Date().toISOString();
        fs.writeFileSync('./messages/' + item.datetime + '.txt', JSON.stringify(item))
    },

};